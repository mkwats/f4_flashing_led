
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "fatfs.h"
#include "i2c.h"
#include "sdio.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "u8g2.h"
#include "filters.h"
#include "math.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint16_t TargetStartDelay = 700;  //ns
uint16_t TargetStopDelay = 1300; //ns
uint8_t iSetEndDelay = 0;
uint8_t iSetStartDelay =0;

/* Private variables ---------------------------------------------------------*/
uint16_t adc1Data[2];
uint16_t adc2Data[1];

int newAvg = 0;
//u8g2 stuff
u8g2_t u8g2_iic;


#define UNUSEDVAR __attribute__ ((unused))

typedef enum {
  HI_WORLD,
  LYDIA_PIC,
  STOCK_U8G2,
  TIMECODE,
  LINES,
  LAST_IMG
} IMGS;

void byte_at_string(uint8_t *str, int i)
{

  str[2] = '0' + i%10;
  str[1] = '0' + (i/10)%10;
  str[0] = '0' + (i/100)%10;

}
void btoa(uint8_t i, uint8_t *str)
{
  str[2] = '0' + i%10;
  str[1] = '0' + (i/10)%10;
  str[0] = '0' + (i/100)%10;
}



void hex_at_string(uint8_t *str, int i)
{
  uint8_t lower = i&0xF;
  uint8_t upper = (i&0xF0) >> 4;

  if (lower < 10) {
    str[1] = '0' + lower;
  } else {
    str[1] = 'A' + lower - 10;
  }
  if (upper < 10) {
    str[0] = '0' + upper;
  } else {
    str[0] = 'A' + upper - 10;
  }
}


int my_I2C_GET_FLAG(I2C_HandleTypeDef* __HANDLE__,
                    uint32_t __FLAG__)
{
	HAL_Delay(100);
  // #define I2C_FLAG_BUSY                   ((uint32_t)0x0010 0002)
  // #define I2C_FLAG_MASK                   ((uint32_t)0x0000 FFFF)
	/*
  if (((uint8_t)((__FLAG__) >> 16)) == 0x01) {
    return (__HANDLE__->Instance->SR1 & __FLAG__ & I2C_FLAG_MASK) == (__FLAG__ & I2C_FLAG_MASK);
  } else {
    return (__HANDLE__->Instance->SR2 & __FLAG__ & I2C_FLAG_MASK) == (__FLAG__ & I2C_FLAG_MASK);
  }
  */
	return 1;
}
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
typedef struct
{
  uint32_t Len;
  char* Data;
} TMsg;

char TMsgData[250];
TMsg msg = {.Data =&TMsgData[0], .Len=0};


//pwm signal stuff
float iDUTY = 0	;
int iSetFreq = 0;
uint16_t iSetFreqMultiplier = 1;

uint32_t amountsent =0;
uint8_t u8x8_byte_my_hw_i2c(
  U8X8_UNUSED u8x8_t *u8x8,
  U8X8_UNUSED uint8_t msg,
  U8X8_UNUSED uint8_t arg_int,
  U8X8_UNUSED void *arg_ptr);

uint8_t u8x8_gpio_and_delay_mine(
u8x8_t *u8x8,
uint8_t msg,
uint8_t arg_int,
void *arg_ptr);

uint8_t USER_UpdatePWM(void);
void Refresh_OLED(u8g2_t *u8g2, IMGS img, char* data);
uint8_t USER_SetFrequency(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_SDIO_SD_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  MX_TIM8_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_FATFS_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */

  /*## Turn ON SD card hardware #################################*/
  //MX_SDIO_SD_Init();
  //HAL_GPIO_WritePin(SD_Power_GPIO_Port, SD_Power_Pin, GPIO_PIN_SET);

/*** Setup OLED *****/
  u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2_iic,
                                         U8G2_R0,
                                         u8x8_byte_my_hw_i2c,
                                         u8x8_gpio_and_delay_mine);
  u8g2_InitDisplay(&u8g2_iic); // send init sequence to the display, display is in sleep mode after this,
  u8g2_SetPowerSave(&u8g2_iic, 0); // wake up display

  Refresh_OLED(&u8g2_iic, HI_WORLD, NULL);

  //turn LED driver on
 // HAL_GPIO_WritePin(LED_DRV_EN_GPIO_Port, LED_DRV_EN_Pin ,GPIO_PIN_SET);


  //PWM signal
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
  iDUTY = 50;

  //tIMER THREE -- DELAY FOR THE MUX
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);

  //ADC
  HAL_TIM_OC_Start(&htim8, TIM_CHANNEL_2);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&adc1Data, 1);

  //Encoder start
  HAL_TIM_Encoder_Start_IT(&htim1,TIM_CHANNEL_1);
 // HAL_TIM_Encoder_Start(&htim1,TIM_CHANNEL_2);

  uint32_t lastCNT = 30000;
  TIM1->CNT = 30000;
  int8_t CNTchange = 0;
  iSetFreq = 50000;
  USER_UpdatePWM();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
//reset the counter
	  CNTchange = (TIM1->CNT- lastCNT);

	  if(abs(CNTchange)>1){
		  CNTchange = CNTchange/2;
		  iSetFreq = (int)(iSetFreq) + (int)(iSetFreqMultiplier* CNTchange);
		  if (iSetFreq <0) {
			  iSetFreq = 0;
		  }
		  lastCNT = TIM1->CNT;
	  }

	  if (USER_UpdatePWM() == 1){
			  enum {BufSize=9};
			  char buf[BufSize];
			  snprintf (buf, BufSize, "%d",  iSetFreq );
			  Refresh_OLED(&u8g2_iic, TIMECODE, buf);

		  }


  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 80;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SDIO|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLQ;
  PeriphClkInitStruct.SdioClockSelection = RCC_SDIOCLKSOURCE_CLK48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	//static int sample[] = {50, 10, 20, 18, 20, 100, 18, 10, 13, 500, 50, 40, 10};
	// the size of this array represents how many numbers will be used
	// to calculate the average
	static int arrNumbers[20] = {0};
	static int pos = 0;
	static long sum = 0;
	int len = 20;


	newAvg = movingAvg(arrNumbers, &sum, pos, 20, (adc1Data[0]/100)*10);
	pos++;
	if (pos >= len){
	  pos = 0;
	}

}

uint8_t
u8x8_byte_my_hw_i2c(
  U8X8_UNUSED u8x8_t *u8x8,
  U8X8_UNUSED uint8_t msg,
  U8X8_UNUSED uint8_t arg_int,
  U8X8_UNUSED void *arg_ptr)
{
#define MAX_LEN 32
  static uint8_t vals[MAX_LEN];
  static uint8_t length=0;

  U8X8_UNUSED uint8_t *args = arg_ptr;
  switch(msg)  {
  case U8X8_MSG_BYTE_SEND: {
    if ((arg_int+length) <= MAX_LEN) {
      for(int i=0; i<arg_int; i++) {
        vals[length] = args[i];
        length++;
      }
    } else {
      uint8_t umsg[] = "MSG_BYTE_SEND arg too long xxx\n";
      byte_at_string(umsg+27, arg_int);
      //sendUARTmsgPoll(umsg, sizeof(umsg));
    }
    uint8_t umsg[] = "MSG_BYTE_SEND 0xxx\n";
    // 00 AE = display off
    // 00 D5 = set display clock
    // 00 80    divide ratio
    // 00 A8 = set multiplex ratio
    // 00 3F    64 mux
    // 00 D3 = set display offset
    // 00 00
    // 00 40 - 00 7F set display start line


    hex_at_string(umsg+16, args[0]);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_INIT: {
    uint8_t umsg[] = "MSG_BYTE_INIT xxx\n";
    byte_at_string(umsg+14, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_SET_DC: {
    uint8_t umsg[] = "MSG_BYTE_SET_DC xxx\n";
    byte_at_string(umsg+15, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_START_TRANSFER: {
    UNUSEDVAR uint8_t umsg[] = "MSG_BYTE_START\n";
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    length = 0;
    break;
  }
  case U8X8_MSG_BYTE_END_TRANSFER: {
    UNUSEDVAR uint8_t umsg[] = "MSG_BYTE_END xxxxx\n";
    itoa(length, (char*)umsg+13,10);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    while(HAL_I2C_GetState (&hi2c1) != HAL_I2C_STATE_READY) { /* empty */ }
    const uint8_t addr = 0x78;
    HAL_I2C_Master_Transmit(&hi2c1, addr, vals, length, 10);
    amountsent+= length;
    break;
  }
  default: {
    uint8_t umsg[] = "MSG_BYTE_DEFAULT xxx\n";
    byte_at_string(umsg+17, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    return 0;
  }
  }
  return 1;
}

uint8_t
u8x8_gpio_and_delay_mine(
u8x8_t *u8x8,
uint8_t msg,
uint8_t arg_int,
void *arg_ptr)
{

  switch(msg)
  {
    case U8X8_MSG_GPIO_AND_DELAY_INIT:
      /* only support for software I2C*/
      break;

    case U8X8_MSG_DELAY_NANO:
      /* not required for SW I2C */
      break;

    case U8X8_MSG_DELAY_10MICRO:
      /* not used at the moment */
      break;

    case U8X8_MSG_DELAY_100NANO:
      /* not used at the moment */
      break;

    case U8X8_MSG_DELAY_MILLI:
      //delay_micro_seconds(arg_int*1000UL);
      break;

    case U8X8_MSG_DELAY_I2C:
      /* arg_int is 1 or 4: 100KHz (5us) or 400KHz (1.25us) */
      //delay_micro_seconds(arg_int<=2?5:1);
      break;

    case U8X8_MSG_GPIO_I2C_CLOCK:
      break;

    case U8X8_MSG_GPIO_I2C_DATA:
      break;
/*
    case U8X8_MSG_GPIO_MENU_SELECT:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_SELECT_PORT, KEY_SELECT_PIN));
      break;
    case U8X8_MSG_GPIO_MENU_NEXT:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_NEXT_PORT, KEY_NEXT_PIN));
      break;
    case U8X8_MSG_GPIO_MENU_PREV:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_PREV_PORT, KEY_PREV_PIN));
      break;

    case U8X8_MSG_GPIO_MENU_HOME:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_HOME_PORT, KEY_HOME_PIN));
      break;
*/
    default:
      //u8x8_SetGPIOResult(u8x8, 1);
      break;
  }
  return 1;
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	static uint8_t PB_counter = 1; 			//push button counter for cycle EXTI
	static uint32_t IntTimeLastPIN10 = 0;
	uint32_t 	IntTimeNow;

	if (GPIO_Pin == GPIO_PIN_10){
		//set the time now for software debounce
		IntTimeNow = HAL_GetTick();

		/* Manage software debouncing*/
		/* If I receive a button interrupt after more than 300 ms from the first one I get it, otherwise I discard it */
		if ((IntTimeNow - IntTimeLastPIN10) > 300U)
		{
			IntTimeLastPIN10 = IntTimeNow;  //reset the last event time
			switch (PB_counter) {
				case 0:
					iSetEndDelay = 0;
					iSetFreqMultiplier = 1;
					PB_counter ++;
					break;
				case 1:
					iSetFreqMultiplier = 10;
					PB_counter ++;
					break;
				case 2:
					iSetFreqMultiplier = 100;
					PB_counter ++;
					break;
				case 3:
					iSetFreqMultiplier = 1000;
					PB_counter ++;
					break;
				case 4:
					iSetFreqMultiplier = 10000;
					PB_counter = 0;
					break;
				default:
					break;
			}
		}
	 }
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim){
	if (TIM1->CR1 & 0x10) { //dir == 1
		//iSetFreq--;
	}else{
		//iSetFreq++;
	}

}

uint8_t USER_SetFrequency(void){
	int timerCounter =__HAL_TIM_GET_COUNTER(&htim2);

	//use the encoder, to step through setting
	switch (timerCounter) {
		case 0:
			//set to 1kHz
			break;

		case 1:
			//set to 10kHz
			 break;


		case 3:
			//set to 100kHz
			break;

		default:
			break;
	}

	  return 1;
}

uint8_t USER_UpdatePWM(void){
static int previousFreq = 0;
	if (previousFreq == iSetFreq) return 1;
	previousFreq = iSetFreq;
/*
Set timer prescaller
Timer count frequency is set with
timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)

    timer_tick_frequency = 80000000 / (0 + 1) = 80000000
    //check the clock frequency

PWM_frequency = timer_tick_frequency / (TIM_Period + 1)
If you know your PWM frequency you want to have timer period set correct
TIM_Period = timer_tick_frequency / PWM_frequency - 1

	for 10Khz PWM_frequency, set Period to
	TIM_Period = 80000000 / 10000 - 1 = 7999
*/
	if(iSetFreq ==0){
		//turn it off
		return 0;
	}
	uint32_t clkFreq = HAL_RCC_GetPCLK2Freq();

	const uint16_t maxAAR = 60000; //maximum reload register value
	//choose PSC
	uint16_t iPSC = (int)(clkFreq/(iSetFreq*(maxAAR+1)));//-1);
	//now based on this iPSC choose a AAR
	uint16_t iAAR = clkFreq/((iPSC+1)*iSetFreq); //-1;

	///but wait needs to be a multiple of the solution... so need a loop to get closer...
	//this just sets the maximum bounds to look for solution.
	//for PWM have duty (can later make dynamic
	//pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
	//where DutyCycle is in percent, between 0 and 100%
	uint32_t iCCR = ((iAAR + 1) * iDUTY) / 100 - 1;

	//now change the timer values

	/* Set autoreload */
	TIM2->CNT = 0;
	TIM2->PSC = iPSC; //__HAL_TIM_SET_PRESCALER(&htim2, iPSC);
	TIM2->ARR = iAAR;	//__HAL_TIM_SET_AUTORELOAD(&htim2, iAAR);
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, iCCR);
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, iCCR);  //can do a differnt one if needed.



	uint32_t nSecPerTick =10000000000/((clkFreq)/(iPSC+1));
	uint32_t TIM3_iCCR =(10*TargetStartDelay)/(nSecPerTick);
	uint32_t TIM3_iAAR = iCCR+((10*TargetStopDelay)/(nSecPerTick));



	TIM3->CNT = 0;
	__HAL_TIM_SET_PRESCALER(&htim3, iPSC); //TIM3->PSC = iPSC; //_
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, TIM3_iCCR);
	__HAL_TIM_SET_AUTORELOAD(&htim3, TIM3_iAAR); //TIM3->ARR = TIM3_iAAR;	//__HAL_TIM_SET_AUTORELOAD(&htim2, iAAR);


	return 1;

}

void Refresh_OLED(u8g2_t *u8g2, IMGS img, char* data)
{
	enum {BufSize=9};
	char buf[BufSize];

	u8g2_ClearBuffer(u8g2);

	u8g2_SetFont(u8g2, u8g2_font_ncenB12_tr);
	u8g2_DrawStr(u8g2, 0,12,"photodoide");

	u8g2_SetFont(u8g2, u8g2_font_ncenB12_tr); //u8g2_font_fub20_tn);
	if(adc1Data[0] >4000){
	u8g2_DrawStr(u8g2, 0,35, "OFF"); //data
	}else {
	snprintf (buf, BufSize, "%i", iSetFreqMultiplier); //newAvg); // adc1Data[0] );
	u8g2_DrawStr(u8g2, 0,35, buf); //data
	snprintf (buf, BufSize, "%i", iSetFreq); //newAvg); // adc1Data[0] );
	u8g2_DrawStr(u8g2, 0,55, buf); //data
	}


	//u8g2_SetFont(u8g2, u8g2_font_fur11_tn); //u8g2_font_fub20_tn);
//	snprintf (buf, BufSize, "%i",  adc1Data[1] );
//	u8g2_DrawStr(u8g2, 0,55, buf); //data

	u8g2_DrawBox(u8g2, 120,0,10,40); //box on the right was used for testing
	u8g2_SendBuffer(u8g2);

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
