# f4_flashing_led

This project flashes a LED used to test the bandwidth of my photodiode circuits.
It is made for the Nucleo F4469 you will need to adopt it for other boards.

It includes a 
   - SD (why? because my display board has one), 
   - I2C display (to display the frequency of the LED pulse), 
   - rotarty encoder with pushbutton (to set the frequency of the LED),

To use wire up your bits and turn the encoder to set the frequnecy, push the encoder button to scroll between 1, 10, 100, 1000, 10000Hz incrments on a encoder pulse.

### Installation

To use it you need to generate the code from CubeMX and open the generated project in System Workbench.
A powerpoint file is incldued with the IO used.

### Development

Want to contribute? Great!


### Todos

 - Write MORE Tests
 - Add another timer with one shot mode.. need UI changes too

License
----

MIT


**Free Software, Hell Yeah!**

